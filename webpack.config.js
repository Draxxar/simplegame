const path = require('path');
const LiveReloadPlugin = require('webpack-livereload-plugin');

module.exports = {
    entry: {
        game: './Modules/GameController.js',
        tstest: './Modules/TypeScriptTest.ts'
    },
    output: {
        path: path.resolve(__dirname, './webpack'),
        filename: '[name].bundle.js',
        libraryTarget: 'var',
        library: '[name]Bundle'
    },
    resolve: {
        // Add `.ts` as a resolvable extension.
        extensions: [".ts", ".js"]
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env']
                    }
                }
            },
            {
                test: /\.ts$/,
                exclude: /node_modules/,
                use: {
                    loader: 'ts-loader',
                    options: {
                        transpileOnly: true
                    }
                }
            }
        ]
    },
    optimization: {
        minimize: false
    },
    plugins: [
        new LiveReloadPlugin([])
    ]
};
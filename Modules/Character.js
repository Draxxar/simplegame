export default class Character {
    constructor(name, type) {
        this.setName(name);
        this.setType(type)
    }

    getName() {
        return this.name;
    }

    setName(newName) {
        this.name = newName;
    }

    getType() {
        return this.type;
    }

    setType(type) {
        this.type = type;
    }

    showCharacterInfo() {
        return "Name: " + this.name + ' Type:' + this.type;
    }

    static showWelcomeText() {
        return "Welcome!";
    }
}


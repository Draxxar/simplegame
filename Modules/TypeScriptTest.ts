interface Person {
    firstName: string;
    lastName: string;
}

class TStestController
{
    private age: number = 18;


    fullName: string;
    constructor (public firstName: string, public middleInitial: string, public lastName: string) {
        this.fullName = firstName + " " + middleInitial + " " + lastName;
    };

    hello (person: Person): void
    {
        console.log('hello ' + person.firstName + ' ' + person.lastName);
        console.log('asdadasd');
    }

    callFullName(): void
    {
        console.log(this.fullName);
        console.log(this.age);
        console.log(this.addNumbers(1,3));
        console.log(this.addNumbers(1,3, 6));
        console.log(this.identity('asd'));
        console.log(this.identity(1));
        console.log(this.loggingIdentity([1,2,'asd']));
        console.log(this.loggingIdentity2([1,2,'asd']));

    }

    addNumbers (a:number, b:number, c:number = 1): number
    {
        console.log('c: ' + c);
        return a + b + c;
    }

    //Generyk
    identity<T>(arg: T): T {
        return arg;
    }
     loggingIdentity<T>(arg: T[]): T[] {
        console.log(arg.length);  // Array has a .length, so no more error
        return arg;
    }

    loggingIdentity2<T>(arg: Array<T>): Array<T> {
        console.log(arg.length);  // Array has a .length, so no more error
        return arg;
    }
}

export const tstestController = new TStestController('Super', 'Kolo', 'Nowak');
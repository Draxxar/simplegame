import Player from './Player';
import Enemy from './Enemy';

class GameController {
    constructor() {
        this.player = null;
        this.enemy = null;
        this.playerType = 'player';
        this.enemyType = 'enemy';
    }

    createPlayer(name) {
        this.player = new Player(name, this.playerType);
    }
    createEnemy() {
        const enemyName = "Ultra Wściekły Goblin";
        this.enemy = new Enemy(enemyName, this.enemyType);
    }
}

export const gameController = new GameController();